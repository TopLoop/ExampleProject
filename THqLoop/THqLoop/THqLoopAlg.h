/** @file  THqLoopAlg.h
 *  @brief THqLoopAlg class header
 *  @class THqLoopALg
 *  @brief TopLoop based algorithm for tHq analysis
 *
 * A class to process SingleTop ntuples for tHq, utilizing the TopLoop
 * framework.
 *
 * @author tHq analysis team
 */

#ifndef THqLoop_THqLoopAlg_h
#define THqLoop_THqLoopAlg_h

// TopLoop
#include <TopLoop/Core/Algorithm.h>
#include <TopLoop/Core/SampleMetaSvc.h>

// ROOT
#include <TFile.h>

namespace thq {

  class THqLoopAlg : public TL::Algorithm {
  private:
    // we're going to set up some simple ntuple output we give some
    // default names "unnamed" - we'll let the user of the class name
    // the files however they like.
    std::string m_outFileName{"unnamed_file.root"};
    std::string m_outTreeName{"unnamed_tree"};
    std::unique_ptr<TFile> m_outFile{nullptr};
    TTree* m_outTree{nullptr};

    /////////////////////////////////////////
    //// sum variables we want to save //////

    // information necessary for lumi weighting
    float m_totalSumWeights;
    float m_sampleXsec;
    float m_1ifbScale;

    // the nominal weight for each event
    float m_weight_nominal;
    // some of the systematic weights
    float m_weight_sys_pileup_UP;
    float m_weight_sys_pileup_DOWN;

    // some event properties
    float m_pt_lep1;
    float m_pt_lep2;
    unsigned int m_njets;

    /////////////////////////////////////////

  public:

    // setting up a standard C++ class
    THqLoopAlg();
    virtual ~THqLoopAlg();

    // these are the functions which are _absolutely necessary_ when
    // using TopLoop. TopLoop's Job runner class calls these functions
    // for you when you run your algorithm
    TL::StatusCode init() final;
    TL::StatusCode setupOutput() final;
    TL::StatusCode execute() final;
    TL::StatusCode finish() final;

    /// set the output ROOT file's name
    void setOutFileName(const std::string name) { m_outFileName = name; }

  };
}

#endif
